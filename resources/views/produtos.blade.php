@extends('layouts.dashboard', ["current" => "produtos"])

@section('conteudo')
<div class="page-wrapper">
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-md-5">
         <h4 class="page-title">PRODUTOS</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page"><a href="/admin">Home (Dashboard)</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Produtos</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="col-md-7">
         <div class="text-right upgrade-btn">
            <a href="/admin/produtos/novo" class="btn btn-success text-white"><i class="fa fa-plus-square"></i> NOVO PRODUTO</a>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="card produtos">
            @include('componentes.tabela-produtos')
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-6">
         <div class="card">
            <div class="card-body">
               <div class="d-md-flex">
                  <div>
                     <h4 class="card-title">LISTA DOS PRODUTOS DESATIVADOS</h4>
                     <h5 class="card-subtitle">Exibindo o total de <span id="total"></span> produto(s) desativado(s).</h5>
                  </div>
                  <div class="ml-auto d-flex no-block align-items-center">
                     <ul class="list-inline font-12 dl m-r-15 m-b-0">
                        <li class="list-inline-item"><i class="mdi mdi-check text-success"></i> ATIVA O PRODUTO</li>
                     </ul>
                  </div>
               </div>
               <div class="row">
                     <div class="card-body o-auto" style="height: 15.8rem">
                        <div class="table-responsive">
                           <table class="table v-middle text-nowrap">
                              <tbody id="desativados">
                                  <!-- ITENS DA TBL INSERIDOS DINAMICAMENTE PELO JS -->
                              </tbody>
                           </table>
                        </div>
                     </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">CONTROLE DE PRODUTOS (TOTAIS)</h4>
               <div class="feed-widget">
                  <ul class="list-style-none feed-body m-0 p-b-20">
                     <li class="feed-item">
                        <div class="feed-icon bg-info">
                           <i class="mdi mdi-account"></i></div>
                        <span id="totalClientes"></span>
                        <span class="ml-auto font-12 text-muted">
                           <span id="dataUltimo"></span></span>
                     </li>
                     <li class="feed-item">
                        <div class="feed-icon bg-danger">
                           <i class="mdi mdi-block-helper"></i></div>
                        <span id="totalDesativados"></span>
                        <span class="ml-auto font-12 text-muted">
                           <span id="dataDesativados"></span></span>
                     </li>
                     <li class="feed-item">
                        <div class="feed-icon bg-warning">
                           <i class="mdi mdi-map-marker"></i></div>
                        <span id="semEndereco"></span>
                        <span class="ml-auto font-12 text-muted">
                        <span id="dataSemEndereco"></span></span>
                     </li>
                     <li class="feed-item">
                        <div class="feed-icon bg-success"><i class="mdi mdi-cart"></i></div>
                        New user registered.<span class="ml-auto font-12 text-muted">30 May</span>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   @if (session('OK'))
      <div class="alert alerta-sucesso alert-dismissible" role="alert">
         <i class="fas fa-check-circle"></i>{{ session('OK') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
   @endif
@endsection

@section('js')
<script type="text/javascript">
// PAGINACAO
$(function () {
    $('body').on('click', '.pagination a', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url
        }).done(function (data) {
            $('.produtos').html(data);
        });
        //window.history.pushState("", "", url);      
    });
});
// FIM DA PAGINACAO

// CARREGA OUTROS DADOS VINDOS DA CONTROLLER
/**function carregaDesativados() {
    $.getJSON('/admin/desativados', function (data) {        
        for (i = 0; i < data.length; i++) {
            var linha = '<tr><td><a href="" class="text-secondary"><img src="/storage/' +
                data[i].foto + '" class="rounded-circle" width="40" height="40" />&nbsp;&nbsp;<strong>' +
                data[i].name + '</strong></a></td><td>' + data[i].email + '</td><td>' +
                '<a href="/admin/ativar/' + data[i].id +
                '" class="text-success" title="Ativar"><i class="mdi mdi-check"></i></a></td></tr>';
            $('#desativados').append(linha);
        }
        $('#total').html(data.length);
    });
}

function carregaControles() {
    $.getJSON('/admin/controles', function (data) {
        totalClientes = 'CLIENTES: <strong>' + data.totalClientes + '</strong>';
        dataUltimo = data.dataUltimo;
        totalDesativados = 'DESATIVADOS: <strong>' + data.totalDesativados + '</strong>';
        dataDesativados = data.dataDesativados;
        semEndereco = 'INCOMPLETOS: <strong>' + data.semEndereco + '</strong>';
        $('#totalClientes').html(totalClientes);
        $('#dataUltimo').html(dataUltimo);
        $('#totalDesativados').html(totalDesativados);
        if (dataDesativados === "01/01/1970") {
            $('#dataDesativados').html("&#8212;");
        } else {
            $('#dataDesativados').html(dataDesativados);
        }
        $('#semEndereco').html(semEndereco);
        $('#dataSemEndereco').html(dataUltimo);
    });
}

$(function () {
    carregaDesativados();
    carregaControles();
});*/
// FIM DO CARREGAMENTO DE OUTROS DADOS
</script>
@endsection

<div class="card-body">
   <div class="d-md-flex">
      <div>
         <h4 class="card-title">LISTA DOS FORNECEDORES ATIVOS</h4>
         <h5 class="card-subtitle">Exebindo {{$fornecedores->count()}}
         fornecedores(s) de {{$fornecedores->total()}}
            ({{$fornecedores->firstItem()}} a {{$fornecedores->lastItem()}}).
         </h5>
      </div>
      <div class="ml-auto d-flex no-block align-items-center">
         <ul class="list-inline font-12 dl m-r-5 m-b-3">
            <li class="list-inline-item"><i class="mdi mdi-pencil text-info"></i> EDIÇÃO DOS DADOS DO FORNECEDOR</li>
            <li class="list-inline-item"><i class="mdi mdi-block-helper text-danger"></i> DESATIVA O FORNECEDOR</li>
         </ul>
      </div>
   </div>
   <div class="d-md-flex justify-content-end">
      <ul class="list-inline m-r-5 m-b-0">
         <li>
            <form method="POST" action="/admin/fornecedores/busca">
            @csrf
            <div class="input-group stylish-input-group">
               <input type="search" class="form-control form-control-sm"
                  placeholder="BUSCAR FORNECEDOR" name="aPesquisar"
                  id="aPesquisar" requerid />
               <span class="input-group-addon">
               <button type="submit" id="pesquisar">
               <i class="mdi mdi-magnify"></i>
               </button>
               </span>
            </div>
            </form>
         </li>
         <li></li>
      </ul>
   </div>
</div>
<div class="table-responsive tamanho-tbl">
   <table class="table v-middle text-nowrap">
      <thead>
         <tr class="bg-light">
            <th class="border-top-0">RAZÃO SOCIAL</th>
            <th class="border-top-0">CNPJ</th>
            <th class="border-top-0">E-MAIL</th>
            <th class="border-top-0 text-center">TELEFONE</th>
            <th class="border-top-0 text-center">SITE</th>
            <th class="border-top-0 text-center">AÇÕES</th>
         </tr>
      </thead>
      <tbody>
         @foreach($fornecedores as $f)
         <tr>
            <td><a href="/admin/fornecedores/perfil/{{$f->id}}" class="text-secondary">
               <img src="/storage/{{$f->foto}}" class="rounded-circle" width="40" height="40" />
               &nbsp;<strong>{{$f->razaosocial}}</strong></a>
            </td>
            <td>{{$f->cnpj}}</td>
            <td>
            @if($f->email != "—")
            <a href="malito:{{$f->email}}" class="text-info">
            {{$f->email}}</a>
            @else
            {{$f->email}}
            @endif
            </td>
            <td class="text-center">{{$f->telefone}}</td>
            <td class="text-center">
            @if($f->site != "—")
            <a href="{{$f->site}}" class="text-secondary" target="_blank">
            <i class="mdi mdi-web"></i></a>
            @else
            {{$f->site}}
            @endif
            </td>
            <td class="text-center">
               <a href="/admin/fornecedores/editar/{{$f->id}}" class="text-info"
                  title="Editar"><i class="mdi mdi-pencil"></i></a>&nbsp;
               <a href="/admin/fornecedores/desativar/{{$f->id}}" class="text-danger" title="Desativar"><i class="mdi mdi-block-helper"></i></a>
            </td>
         </tr>
         @endforeach
      </tbody>
   </table>
</div>
<div class="paginacao">
   {{$fornecedores->links()}}
</div>

      
<aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar">
               <nav class="sidebar-nav">
                  <ul id="sidebarnav" class="in">
                     <!-- PRIMEIRO ITEM DO MENU LATERAL (PERFIL DO USUARIO)-->                      
                     <li>
                        <div class="user-profile d-flex no-block dropdown m-t-20">
                           <div class="user-pic"><img src="/storage/{{ Auth::user()->foto }}" 
                              alt="users" class="rounded-circle" width="40" /></div>
                           <div class="user-content hide-menu m-l-10">
                              <a href="javascript:void(0)" class="" id="Userdd" 
                                 role="button" data-toggle="dropdown" 
                                 aria-haspopup="true" aria-expanded="false">
                                 <h5 class="m-b-0 user-name font-medium">
                                    {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
                                 </h5>
                                 <span class="op-5 user-email" 
                                 title="{{ Auth::user()->email }}">{{ Auth::user()->email }}</span>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right" 
                                 aria-labelledby="Userdd">
                                 @if(Auth::user()->perfil === "CLIENTE")
                                 <a class="dropdown-item"href="/home/perfil/{{Auth::user()->id}}">
                                 @else
                                 <a class="dropdown-item" href="/admin/perfil/{{Auth::user()->id}}">
                                 @endif
                                 <i class="ti-user m-r-5 m-l-5"></i> MEU PERFIL</a>
                                 <a class="dropdown-item" href="javascript:void(0)">
                                 <i class="ti-wallet m-r-5 m-l-5"></i> MEU BALANÇO</a>
                                 <a class="dropdown-item" href="javascript:void(0)">
                                 <i class="ti-email m-r-5 m-l-5"></i> CAIXA DE ENTRADA</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="javascript:void(0)">
                                 <i class="ti-settings m-r-5 m-l-5"></i> CONFIGURAÇÕES</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                 <i class="fa fa-power-off m-r-5 m-l-5"></i> SAIR</a>
                                 <form id="logout-form" action="{{ route('logout') }}" 
                                    method="POST" style="display: none;">
                                    @csrf
                                 </form>
                              </div>
                           </div>
                        </div>
                        <!-- FIM DO PERFIL DO USUARIO DO MENU LATERAL ESQUERDO -->
                     </li>
                     <li class="p-15 m-t-10"><a href="javascript:void(0)" 
                        class="btn btn-block create-btn text-white no-block d-flex align-items-center">
                        <i class="fa fa-plus-square"></i> <span class="hide-menu m-l-5">NOVO PEDIDO</span></a>
                     </li>
                     <!-- ITENS DO MENU LATERAL ESQUERDO -->
                     <li @if($current == "admin" || $current == "home") class="sidebar-item selected" @else class="sidebar-item" @endif>
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="/admin" aria-expanded="false">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu">Dashboard</span></a>
                     </li>
                     <li @if($current == "clientes") class="sidebar-item selected" @else class="sidebar-item" @endif>
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="/admin/clientes" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Clientes</span></a>
                     </li>
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="table-basic.html" aria-expanded="false">
                        <i class="mdi mdi-food"></i>
                        <span class="hide-menu">Lanches</span></a>
                     </li>
                     <li @if($current == "fornecedores") class="sidebar-item selected" @else class="sidebar-item" @endif> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="/admin/fornecedores" aria-expanded="false">
                        <i class="mdi mdi-truck"></i>
                        <span class="hide-menu">Fornecedores</span></a>
                     </li>
                     <li @if($current == "produtos") class="sidebar-item selected" @else class="sidebar-item" @endif> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="/admin/produtos" aria-expanded="false">
                        <i class="mdi mdi-package-variant-closed"></i>
                        <span class="hide-menu">Estoque</span></a>
                     </li>
                     <li @if($current == "compras") class="sidebar-item selected" @else class="sidebar-item" @endif> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="/admin/compras" aria-expanded="false">
                        <i class="mdi mdi-shopping"></i>
                        <span class="hide-menu">Compras</span></a>
                     </li>                     
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="error-404.html" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Usuários</span></a>
                     </li>                    
                  </ul>
               </nav>
            </div>
         </aside>
        
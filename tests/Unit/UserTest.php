<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{
    /**
        * @test
    */
    public function deve_logar_usuario(){

        $response = $this->call('POST', '/login', [
            'email' => 'sergio_diasfilho@hotmail.com',
            'password' => 'testes123',
            '_token' => csrf_token()
        ]);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertRedirect('/home');
    }

    /**
        * @test
    */
    public function deve_registrar_novo_usuario(){
        // Cria um usuário
        $novo_usuario = [
            '_token' => csrf_token(),
            'name' => 'Lorem',
            'sobrenome' => 'Ipsum',
            'email' => 'dsss@gmail.com',
            'password' => 'testes123',
            'password_confirmation' => 'testes123'
        ];

        // Cenário -> requisição para /register
        $response = $this->call('POST', '/register', $novo_usuario);

        // Garante que o usuário está no banco de dados;
        $query = User::where('email', $novo_usuario['email'])->first();
        $this->assertTrue((bool) $query);

    }
}

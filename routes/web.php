<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

/*
|--------------------------------------------------------------------------
| COMANDO: php artisan make:auth QUE CRIA ESSAS LINHAS ABAIXO! 
|--------------------------------------------------------------------------
*/
//CRIA UMAS QUANTAS ROTAS OCULTAS AQUI POR ESSA UNICA LINHA DE CODIGO!
Auth::routes();

//CRIA A ROTA P/ ABRIR A PAG HOME D SITE EXEMPLO D LARAVEL DEPOIS D LOGADO!
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/perfil/{id}', 'HomeController@perfil')->name('home.perfil');
Route::post('/home/{id}', 'HomeController@update');

//CRIA A ROTA P/ ABRIR A PAG HOME D SITE EXEMPLO D LARAVEL DEPOIS D LOGADO!
Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
Route::get('/admin/login', 'Auth\AdminLoginController@index')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/admin/perfil/{id}', 'AdminController@perfil')->name('admin.perfil');
Route::post('/admin/{id}', 'AdminController@update');
Route::get('/admin/clientes', 'ClienteController@index');
Route::get('/admin/desativados', 'ClienteController@desativados');
Route::get('/admin/controles', 'ClienteController@controles');
Route::get('/admin/desativar/{id}', 'ClienteController@desativar');
Route::get('/admin/ativar/{id}', 'ClienteController@ativar');
Route::get('/admin/clientes/editar/{id}', 'ClienteController@edit');
Route::post('/admin/clientes/editar/{id}', 'ClienteController@update');
Route::get('/admin/clientes/novo', 'ClienteController@create');
Route::post('/admin/clientes/novo', 'ClienteController@store');
Route::post('/admin/clientes/busca', 'ClienteController@busca');
Route::get('/admin/clientes/procurar', 'ClienteController@procurar');
Route::get('/admin/fornecedores', 'MercadoController@index');
Route::get('/admin/produtos', 'ProdutoController@index');
Route::get('/admin/compras', 'CompraController@index');
Route::post('/admin/compras/nova', 'CompraController@store');

//Route::get('/admin/clientesjson', 'ClienteController@indexjson');

/**Route::prefix('/admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@index')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
});**/

/** Route::get('admin/perfil', function(){*/

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatosusers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cep')->nullable()->default("—");
            $table->string('rua')->nullable()->default("—");
            $table->string('numero')->nullable()->default("—");
            $table->string('complemento')->nullable()->default("—");
            $table->string('bairro')->nullable()->default("—");
            $table->string('cidade')->nullable()->default("—");
            $table->string('uf')->nullable()->default("—");           
            $table->string('celular1')->nullable()->default("—");
            $table->string('celular2')->nullable()->default("—");
            $table->string('residencial')->nullable()->default("—");
            $table->string('facebook')->nullable()->default("—");
            $table->string('twitter')->nullable()->default("—");
            $table->string('instagram')->nullable()->default("—");
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatosusers');
    }
}

![Logo](https://gitlab.com/angelogl/ogroburger/raw/master/outros/imagens/2018-08-24_-_logo_200_x_200.png)

#  Ogro Burger

O "Ogro Burger" é um *software* gratuito, completamente codificado em 
**[Laravel](https://laravel.com/)** (*framework* PHP) que contará com algumas 
outras tecnologias integradas a ele: 
**[jQuery](https://jquery.com/)**, **[Bootstrap 4](https://getbootstrap.com/)**, 
**[MySQL](https://www.mysql.com/)** e 
[outras](https://gitlab.com/angelogl/ogroburger/wikis/5.-tecnologias-utilizadas)... 
Embora ainda seja um projeto bastante jovem, ele oferece uma visão gigante de 
uma aplicação simples usando essas tecnologias e em breve o mesmo será colocado 
em **produção** atendendo as expectativas de um **cliente**, pois o mesmo não se
trata somente de um sistema fictício como requisito da disciplina de 
**Projeto de Desenvolvimento**. O projeto (sistema *web*) basicamente consiste 
em possibilitar o administrador controlar o cadastro de novos usuários 
(clientes) no sistema, a venda de "hamburguês" e todo o controle de estoque, 
para ter mais detalhes da dinâmica do negócio clique 
[aqui](https://gitlab.com/angelogl/ogroburger/wikis/1.-Home).

O projeto “Ogro Burger” começou a ser desenvolvido na aula 01 (atividade única) 
e faz parte da avaliação da disciplina de **Projeto de Desenvolvimento** do 
4° semestre da 
**[Faculdade de Tecnologia Senac Pelotas](https://www.senacrs.com.br/unidades.asp?unidade=78)** 
(RS) e pertence ao [grupo](#grupo):
**Rafael Calearo**, **Caiã Ceron**, **Leandro Santos**, **Yuri Ferreira** e 
**Sérgio Filho**. O projeto conta também com a orientação do prof. 
**[Angelo Luz](https://gitlab.com/angelogl)** e o andamento do mesmo poderá ser 
acompanhado (visto) no repositório do **[Heroku](https://www.heroku.com/)**: 
em breve o "*LINK*" estará aqui!

🚧 **Situação atual do projeto:** em construção/desenvolvimento!

Sinta-se livre para [clonar](#clonar) ou [baixar](#clonar) o código-fonte do 
projeto.

## Pré-requisitos

O **[Laravel](https://laravel.com)** assim como outros *frameworks* necessitam 
de alguns [requisitos de sistema](https://laravel.com/docs/5.6/installation#installation). 
O *site* do **Laravel** sugere que para desenvolver aplicações em **Laravel** o 
ideal é usar a máquina virtual 
**[Laravel Homestead](https://laravel.com/docs/5.6/homestead)**, pois ela atende
a todos esses 
[requisitos de sistema](https://laravel.com/docs/5.6/installation#installation). 
Porém, caso você não use o **Homestead** o recomendado seria o uso da ferramenta 
**[XAMPP](https://www.apachefriends.org/pt_br/index.html)** ou similar, pois é 
fácil de instalar e possui todo o ambiente de desenvolvimento necessário para 
desenvolver aplicações em **Laravel** ou "rodar" aplicações baixadas 
(prontas/desenvolvimento) como essa!

Outra ferramenta que faz parte dos 
[requisitos de sistema](https://laravel.com/docs/5.6/installation#installation) 
é o **[Composer](https://getcomposer.org/)** que serve para gerenciar 
dependências. Nesse **README** não será mostrado como instalar essas ferramentas 
então, fique a vontade em clicar nos *links* fornecidos anteriormente para obter 
a documentação dos mesmos junto aos seus *sites* oficiais ou assista esse 
[vídeo](https://www.youtube.com/watch?v=V1RA7V2Kn8g&lc=z22pxbehrszbc1xag04t1aokgbxwjxqj4o1po2swbyl2rk0h00410) 
no **YouTube**.

⚠ **ATENÇÃO, as ferramentas, comandos, linguagens (*framework*) e etc. requerem conhecimentos prévios!**

## Índice

* [Clonar](#clonar)
* [How to (como usar)](#uso)
* [O grupo](#grupo)
* [Documentação](#documentação)
* [Licença](#licença)

## Clonar

Para efetuar o clone do projeto usando os *links* disponíveis no repositório do 
GitLab é preciso possuir o *software* Git instalado na sua máquina. Para saber 
como instalar e usar a ferramenta Git acesse o *site* do Git: 
<https://git-scm.com/doc>. Caso já saiba como fazer use os comandos abaixo:

**Com SSH:**

```bash
$ git clone git@gitlab.com:angelogl/ogroburger.git
```

ou 

**Com HTTPS:**

```bash
$ git clone https://gitlab.com/angelogl/ogroburger.git
```

ou você pode baixar o projeto (*download*): 
[zip](https://gitlab.com/angelogl/ogroburger/-/archive/master/ogroburger-master.zip),
[tar.gz](https://gitlab.com/angelogl/ogroburger/-/archive/master/ogroburger-master.tar.gz),
[tar.bz2](https://gitlab.com/angelogl/ogroburger/-/archive/master/ogroburger-master.tar.bz2) e 
[tar](https://gitlab.com/angelogl/ogroburger/-/archive/master/ogroburger-master.tar).

## Uso

**ANTES DE USAR:** leia os [pré-requisitos](#pré-requisitos).

Após ter clonado/baixado o projeto, execute os seguintes comandos listados 
abaixo no diretório do projeto usando as ferramentas de sua preferência (cmd 
Windows, terminal do Linux, Git ou IDE):

* `composer install`: serve para ler o arquivo `composer.lock` e instalar as dependências listadas nesse arquivo;
* `composer dump`: serve para recarregar a lista de **classes**, **pacotes** e **bibliotecas** que estão dentro do seu projeto no arquivo de **autoload**;
* `cp .env.example .env`: serve para criar o arquivo `.env` caso o projeto não possua (Linux);
* `copy .env.example .env`: serve para criar o arquivo `.env` caso o projeto não possua (Windows);
* `php artisan key:generate`: serve para gerar uma chave do tipo de segurança para o seu arquivo `.env`;
* `php artisan migrate`: serve para criar as tabelas (arquivos) compostos no projeto no **banco de dados** que estará rodando no momento;
* `php artisan db:seed`: serve para **inserir** registros nas tabelas criadas anteriormente;
* `php artisan serve`: e por fim, esse comando serve para iniciar a aplicação que pode ser visualizada no **navegador** (*browser*).

⚠ **ATENÇÃO, É PRECISO CONFIGURAR O ARQ. .ENV APÓS CRIADO CONFORME O SEU BD, EM 
CASO DE DÚVIDAS ACESSE O 
[VÍDEO](https://www.youtube.com/watch?v=V1RA7V2Kn8g&lc=z22pxbehrszbc1xag04t1aokgbxwjxqj4o1po2swbyl2rk0h00410)!**

## Grupo

O nosso grupo conta com os seguintes membros (mantenedores) e cada um é 
responsável por uma função dentro do processo de desenvolvimento conforme a 
tabela abaixo:

<table>
   <tboby>
      <td align="center" valign="top">
         <a href="https://gitlab.com/rafaelcalearo">
            <img src="https://gitlab.com/angelogl/ogroburger/raw/master/outros/imagens/2018-08-31_-_Rafael_Calearo.png" /></a><br>
         <b>Rafael Calearo<b><br> 
         <a href="https://pt.stackoverflow.com/questions/52450/o-que-%C3%A9-e-o-que-faz-um-full-stack-web-developer"><em>Full Stack developer (líder)</em></a> 
      </td>
      <td align="center" valign="top">
         <a href="https://gitlab.com/leandro2206">
	     <img src="https://gitlab.com/angelogl/ogroburger/raw/master/outros/imagens/2018-08-31_-_Leandro_Santos.png" /></a><br>
	     <b>Leandro Santos<b><br> 
         <a href="https://pt.wikipedia.org/wiki/Front-end_e_back-end"><em>Back-end developer</em></a>
      </td>	
      <td align="center" valign="top">
         <a href="https://gitlab.com/YuriFerreira">
	     <img src="https://gitlab.com/angelogl/ogroburger/raw/master/outros/imagens/2018-08-31_-_Yuri_Ferreira.png" /></a><br>
	     <b>Yuri Ferreira<b><br> 
         <a href="https://pt.wikipedia.org/wiki/Front-end_e_back-end"><em>Front-end developer</em></a>
      </td>	
      <td align="center" valign="top">
         <a href="https://gitlab.com/SergioDias">
	     <img src="https://gitlab.com/angelogl/ogroburger/raw/master/outros/imagens/2018-08-31_-_S%C3%A9rgio_Filho.png" /></a><br>
	     <b>Sérgio Filho<b><br> 
         <a href="https://en.wikipedia.org/wiki/Software_testing"><em>Software tester</em></a>
      </td>
      <td align="center" valign="top">
         <a href="https://gitlab.com/caiaceron">
         <img src="https://gitlab.com/angelogl/ogroburger/raw/master/outros/imagens/2018-08-31_-_Cai%C3%A3_Ceron.png" /></a><br>
	     <b>Caiã Ceron<b><br> 
         <a href="https://en.wikipedia.org/wiki/Software_documentation"><em>Software documentation</em></a>
      </td>
   <tbody>
</table>

## Documentação

A documentação com maior clareza se encontra na 
**[wiki](https://gitlab.com/angelogl/ogroburger/wikis/home)** desse repositório.

## Licença

A aplicação conta com a seguinte licença de uso: 
**[MIT](https://opensource.org/licenses/MIT).**
